/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player PlayerX;
    private Player PlayerO;
    private Player currentPlayer;
    private Player winner;
    private boolean Finish = false;
    private int lastrow;
    private int lastcol;

    public Table(Player X, Player O) {
        PlayerX = X;
        PlayerO = O;
        currentPlayer = X;
    }

    public void showTable() {
        System.out.println(" 1 2 3 ");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + "");
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println(" ");
        }
    }
    public char getRowCol(int row ,int col){
        return table [row][col];
    }

    public boolean setRowCol(int row, int col) {
        System.out.println(isFinish());
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getCuurentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == PlayerX) {
            currentPlayer = PlayerO;
        } else {
            currentPlayer = PlayerX;
        }
    }

    void checkcol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;

            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == PlayerO) {
            PlayerO.win();
            PlayerX.lose();
        } else {
            PlayerO.lose();
            PlayerX.win();
        }
    }

    void checkrow() {
        for (int col = 0; col < 3; col++) {
            if (currentPlayer.getName() != table[lastrow][col]) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkX() {
        if (table[0][2] == currentPlayer.getName() || table[0][0] == currentPlayer.getName()) {
            if (table[2][2] == currentPlayer.getName() || table[1][1] == currentPlayer.getName()) {
                if (table[2][0] == currentPlayer.getName() || table[2][2] == currentPlayer.getName()) {

                    return;
                } else {
                }
                Finish = true;
                winner = currentPlayer;
                setStatWinLose();
            }
//        for (int i = 0; i < 3; i++) {
//            for (int j = 0; j < 3; j--) {
//                if (currentPlayer.getName() == table[i][j]) {
//                    return;
//                } else {
//                }
//            }
//            Finish = true;
//            winner = currentPlayer;
//        }

        }

//    void checkY() {
//        for (int i = 0; i < 3; i++) {
//            for (int j = 0, k = 2; j < 3; j--) {
//                if (table[j][k] != currentPlayer.getWin()) {
//                    return;
//                }
//            }
//            Finish = true;
//            winner = currentPlayer;
//        }
    }

    public void checkwin() {
        checkcol();
        checkrow();
        checkX();
//        checkDraw();

    }

    void checkDraw() {
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                if (table[row][col] == '-') {
                    return;
                }
            }
        }
        Finish = true;
        PlayerO.Draw();
        PlayerX.Draw();
    }

    public boolean isFinish() {
        return Finish;
    }

    public Player getWinner() {
        return winner;
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            int lastRow = 0;
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            int lastCol = 0;
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
}
